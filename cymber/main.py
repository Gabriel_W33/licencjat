import tensorflow as tf
import numpy as np
import gym

import envs

tf.keras.backend.set_floatx('float64')

env = gym.make('CustomEnv-v0')

print(env.action_space)  # Discrete(2)
print(env.observation_space)  # Box(4,)
InputSize = 12
OutputSize = 9
# env.restart()
# done = False
# while not done:
#     env.render()
#     s_, r, done, _ = env.step(1)
#     if done:
#         env.restart()

model = tf.keras.Sequential()

model.add(tf.keras.layers.Dense(100, input_shape=(InputSize,), activation='relu', kernel_initializer='glorot_uniform'))
model.add(tf.keras.layers.Dropout(0.05, seed=565))
model.add(tf.keras.layers.Dense(OutputSize, activation='softmax', kernel_initializer='glorot_uniform'))
model.compile(optimizer=tf.keras.optimizers.Adam(0.01), loss=tf.keras.losses.MSE)

model2 = tf.keras.Sequential()

model2.add(tf.keras.layers.Dense(100, input_shape=(InputSize,), activation='relu', kernel_initializer='glorot_uniform'))
model2.add(tf.keras.layers.Dropout(0.05))
model2.add(tf.keras.layers.Dense(100, input_shape=(100,), activation='relu', kernel_initializer="glorot_uniform"))
model2.add(tf.keras.layers.Dense(OutputSize, activation='softmax', kernel_initializer='glorot_uniform'))
model2.compile(optimizer=tf.keras.optimizers.Adam(0.01), loss=tf.keras.losses.MSE)


def discount_rewards(_reward, gamma=0.8):
    discounted_reward = np.zeros_like(_reward)
    running_add = 0
    for t in reversed(range(0, _reward.size)):
        running_add = running_add * gamma + _reward[t]
        discounted_reward[t] = running_add
    return discounted_reward


scores = []
# np.random.seed(565)
for ep in range(100000):
    s = env.restart()
    state = []
    state2 = []

    reward = []
    reward2 = []

    action = []
    action2 = []

    chose_action = []
    action_2 = []

    score = 0
    done = False
    while not done:
        env.render()
        s = s.reshape([1, 12])
        s2 = s.reshape([1, 12])
        # print(s)
        a = model(s)
        a2 = model2(s2)
        # print(a)
        a_ = np.random.choice(9, p=a.numpy()[0])
        a_2 = np.random.choice(9, p=a2.numpy()[0])
        # print(a_)
        s_, s_2, r, r2, done, _ = env.step(a_, a_2)
        # print("out", r, r2)
        score += r
        # print(score)
        state.append(s)
        state2.append(s2)

        reward.append(r)
        reward2.append(r2)
        # print('nagrody w tab', reward, reward2)

        action.append(a.numpy()[0])
        action2.append(a2.numpy()[0])

        chose_action.append(a_)
        action_2.append(a_2)

        s = s_
        s2 = s_2

    r_ = discount_rewards(np.array(reward))
    # print("dis", r_)
    r_2 = discount_rewards(np.array(reward))

    action = np.array(action)
    action2 = np.array(action2)

    x = np.array(state).reshape(len(state), InputSize)
    # print(x)
    x2 = np.array(state2).reshape(len(state2), InputSize)

    y = action.reshape((len(chose_action), OutputSize))

    # y2 = np.zeros((len(action_2), OutputSize))
    # r_ = r_.reshape(len(r_), 1)
    # print('re', r_)
    # r_2 = r_2.reshape(len(r_2), 1)

    y[np.arange(len(chose_action)), chose_action] = 1

    # print(y[np.arange(len(chose_action)), chose_action])
    # y2[np.arange(len(action_2)), action_2] = 1
    # r_ = r_.reshape(len(r_))
    # print('rere', r_)
    # r_2 = r_2.reshape(len(r_2))
    # print("nagrody", r_, r_2)

    model.fit(x=x, y=y, sample_weight=r_, verbose=2)
    # model2.fit(x=x2, y=y2, sample_weight=r_2, verbose=0)
    # print(model.get_weights())
    scores.append(score)
    # if ep % 50 == 0:
    #     print(np.mean(np.array(scores[-50:])))
    # if np.mean(np.array(scores[-50:])) >= 490:
    #     model.save('C:\programowanie\cymber'.format(ep))
    #
    #     print('Success in ', ep)
    #     break
