import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
from envs.custom_env_dir.objects import Objects, Ball, Players


class CustomEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 60
    }

    def __init__(self):

        high = np.array([
            2,
            np.finfo(np.float32).max,
            2 * 2,
            np.finfo(np.float32).max])

        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)

        self.agent = Players(200, 400, 20, 200, (0, 0, 255))
        self.enemy = Players(800, 400, 20, 200, (255, 0, 0))
        self.ball = Ball(200, 200, 20, 20, (0, 0, 0))
        # self.ball2 = Ball(700, 500, 20, 20, (0, 255, 0))
        # self.ball3 = Ball(200, 500, 20, 10, (0, 100, 10))
        # self.ball3.vx = 1000
        # self.ball3.vy = 300
        # self.ball2.xy = 500
        # self.ball2.vy = 900

        self.gate = 0.3
        self.band = (100, 1100, 100, 700)
        d = (self.band[3] - self.band[2]) / 2
        self.gate_bottom = d - (d * self.gate) + self.band[0]
        self.gate_up = self.gate_bottom + (2 * d * self.gate)

        self.max_goal = 2
        self.last_goal_is_r = True

        self.seed()
        self.viewer = None
        self.state = None
        self.state2 = None
        self.timer = 0

        self.steps_beyond_done = None

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def count_state(self, p1=True):

        def range_to_ob(this, other):
            dx = other.x - this.x
            dy = other.y - this.y
            return dx, dy

        def range_to_band(this, band):
            l = this.x - band[0]
            r = band[1] - this.x
            b = this.y - band[2]
            u = band[3] - this.y
            return u, b, r, l

        def range_to_gate(this, band, gate_bottom, gate_up):
            lx = band[0] - this.x
            rx = band[1] - this.x
            uy = gate_up - this.y
            by = gate_bottom - this.y
            return lx, rx, uy, by

        if p1:

            d_ball_x, d_ball_y = range_to_ob(self.agent, self.ball)
            d_enemy_x, d_enemy_y = range_to_ob(self.agent, self.enemy)
            d_band_up, d_band_bottom, d_band_left, d_band_right = range_to_band(self.agent, self.band)

            d_left_gate_x, d_right_gate_x, d_gate_up_y, d_gate_bottom_y = range_to_gate(self.agent, self.band,
                                                                                        self.gate_bottom, self.gate_up)

            self.state = [d_ball_x, d_ball_y,
                          d_enemy_x, d_enemy_y,
                          d_band_up, d_band_bottom, d_band_left, d_band_right,
                          d_left_gate_x, d_right_gate_x, d_gate_up_y, d_gate_bottom_y
                          ]
        else:
            d_ball_x, d_ball_y = range_to_ob(self.enemy, self.ball)
            d_enemy_x, d_enemy_y = range_to_ob(self.enemy, self.agent)
            d_band_up, d_band_bottom, d_band_left, d_band_right = range_to_band(self.agent, self.band)

            d_left_gate_x, d_right_gate_x, d_gate_up_y, d_gate_bottom_y = range_to_gate(self.enemy, self.band,
                                                                                        self.gate_bottom, self.gate_up)

            self.state2 = [d_ball_x, d_ball_y,
                           d_enemy_x, d_enemy_y,
                           d_band_up, d_band_bottom, d_band_left, d_band_right,
                           d_left_gate_x, d_right_gate_x, d_gate_up_y, d_gate_bottom_y
                           ]

    def step(self, action1, action2, ):
        # assert self.action_space.contains(action), "%r (%s) invalid" % (action, type(action))
        from pyglet.window import key

        print('action' ,action1)
        # TODO punish for leizzy
        reward1 = 0
        reward2 = 0

        def move(player, r):

            if r == 0:
                player.move('u')
            elif r == 1:
                player.move('d')
            elif r == 2:
                player.move('r')
            elif r == 3:
                player.move('l')
            elif r == 4:
                player.move('ur')
            elif r == 5:
                player.move('ul')
            elif r == 6:
                player.move('dr')
            elif r == 7:
                player.move('dl')
            elif r == 8:
                player.move('')

        def bot(player, band, ball, gate='r', ON=False):
            cross_centr = ball.x + ball.r >= (band[1] + band[0]) / 2
            cross_quartil = ball.x + ball.r >= ((band[1] + band[0]) / 2) + ((band[1] + band[0]) / 4)
            ofe = False
            # TODO remove hardcode posicion
            if ball.vx < 1000 or ball.vy < 1000:
                ofe = False
            if not ON:
                return None
            if gate == 'r':
                defence_posicion = 1000

                if ofe:
                    if (cross_centr and cross_quartil) or not cross_centr:
                        # defense
                        if ball.y > player.y:
                            if player.x < defence_posicion:
                                player.move('ur')
                            elif player.x > defence_posicion:
                                player.move('ul')
                            else:
                                player.move('u')
                        elif ball.y < player.y:
                            if player.x < defence_posicion:
                                player.move('dr')
                            elif player.x > defence_posicion:
                                player.move('dl')
                            else:
                                player.move('d')
                        else:
                            if player.x < defence_posicion:
                                player.move('r')
                            elif player.x > defence_posicion:
                                player.move('l')
                            else:
                                player.move('')

                    elif cross_centr:
                        if ball.y > player.y:
                            if player.x < ball.x:
                                player.move('ur')
                            elif player.x > ball.x:
                                player.move('ul')
                            else:
                                player.move('u')
                        elif ball.y < player.y:
                            if player.x < ball.x:
                                player.move('dr')
                            elif player.x > ball.x:
                                player.move('dl')
                            else:
                                player.move('d')
                        else:
                            if player.x < ball.x:
                                player.move('r')
                            elif player.x > ball.x:
                                player.move('l')
                            else:
                                player.move('r')
                else:
                    if (cross_centr and cross_quartil) or not cross_centr:
                        # defense
                        if ball.y > player.y:
                            if player.x < defence_posicion:
                                player.move('ur')
                            elif player.x > defence_posicion:
                                player.move('ul')
                            else:
                                player.move('u')
                        elif ball.y < player.y:
                            if player.x < defence_posicion:
                                player.move('dr')
                            elif player.x > defence_posicion:
                                player.move('dl')
                            else:
                                player.move('d')
                        else:
                            if player.x < defence_posicion:
                                player.move('r')
                            elif player.x > defence_posicion:
                                player.move('l')
                            else:
                                player.move('')

                    elif cross_centr:
                        if ball.y > player.y:
                            if player.x < ball.x:
                                player.move('ul')
                            elif player.x > ball.x:
                                player.move('ur')
                            else:
                                player.move('u')
                        elif ball.y < player.y:
                            if player.x < ball.x:
                                player.move('dl')
                            elif player.x > ball.x:
                                player.move('dr')
                            else:
                                player.move('d')
                        else:
                            if player.x < ball.x:
                                player.move('r')
                            elif player.x > ball.x:
                                player.move('l')
                            else:
                                player.move('r')

        # def key_press(k, mod):
        #
        #     if k == key.NUM_4: self.enemy.move('l')
        #     if k == key.NUM_6: self.enemy.move('r')
        #     if k == key.NUM_8: self.enemy.move('u')
        #     if k == key.NUM_2: self.enemy.move('d')
        #
        #     if k == key.NUM_9: self.enemy.move('ur')
        #     if k == key.NUM_7: self.enemy.move('ul')
        #     if k == key.NUM_3: self.enemy.move('dr')
        #     if k == key.NUM_1: self.enemy.move('dl')
        #
        # def key_release(k, mod):
        #     if k == key.NUM_8:
        #

        # self.viewer.window.on_key_release = key_release
        # if self.timer >= 1000:
        #     self.reset()
        #     print('times up')
        #     reward1 = -20.0
        #     reward2 = -20.0

        self.count_state(True)
        self.count_state(False)

        move(self.agent, action1)
        bot(self.enemy, self.band, self.ball, 'r', True)
        # move(self.enemy, action2)

        self.ball.step(2.5, 0.01)
        # self.ball2.step(2.5, 0.01)

        # self.ball3.step(2.5, 0.01)
        # self.viewer.window.on_key_press = key_press

        self.agent.band_collision(self.band, 'l')
        self.enemy.band_collision(self.band, 'r')

        goal = self.ball.band_collision(self.band, self.gate)
        # self.ball2.band_collision(self.band)
        # self.ball3.band_collision(self.band)

        if Objects.if_collision(self.ball, self.agent):
            reward1 = 5.0

            Objects.ball_collision(self.ball, self.agent)

        if Objects.if_collision(self.ball, self.enemy):
            reward2 = 5.0
            Objects.ball_collision(self.ball, self.enemy)

        # if Objects.if_collision(self.ball, self.ball2):
        #     Objects.ball_collision(self.ball, self.ball2)
        # if Objects.if_collision(self.ball2, self.ball3):
        #     Objects.ball_collision(self.ball2, self.ball3)
        # if Objects.if_collision(self.ball, self.ball3):
        #     Objects.ball_collision(self.ball, self.ball3)

        done = False

        # if self.agent.points >= self.max_goal:
        #     done = True
        #     reward1 = 100.0
        #     reward2 = -200.0
        # elif self.enemy.points >= self.max_goal:
        #     done = True
        #     reward1 = -200.0
        #     reward2 = 100.0

        # if not done:
        if goal == 'goal_right':
            reward1 = 10.0
            reward2 = -20.0
            timer = 0
            self.agent.points += 1
            self.last_goal_is_r = True
            done = True
            self.reset()

        elif goal == "goal_left":
            reward1 = -20.0
            reward2 = 10.0
            timer = 0
            self.enemy.points += 1
            self.last_goal_is_r = False
            done = True
            self.reset()
        self.timer += 1
        print("nag1", reward1,reward2)
        return np.array(self.state), np.array(self.state2), reward1, reward2, done, {}

    def reset(self):

        self.agent.reset()
        self.enemy.reset()
        self.ball.reset(self.last_goal_is_r)

        return np.array(self.state)

    def restart(self):
        self.agent.reset(True)
        self.enemy.reset(True)
        self.ball.reset(True)

        self.count_state(True)
        self.count_state(False)

        return np.array(self.state)

    def render(self, mode='human'):
        screen_width = 1200
        screen_height = 800
        scale = screen_width

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(screen_width, screen_height)
            band = self.band
            l = (band[0] - band[1]) / 2  # -500
            r = (band[1] - band[0]) / 2  # 500
            t = (band[2] - band[3]) / 2  # 300
            b = (band[3] - band[2]) / 2  # -300

            render_band = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            render_band.set_color(255, 50, 0)
            trans_band = rendering.Transform()
            render_band.add_attr(trans_band)
            trans_band.set_translation(screen_width / 2, screen_height / 2)

            render_gate1 = rendering.make_polyline([(self.band[0], self.gate_bottom), (self.band[0], self.gate_up)])
            render_gate1.set_color(0, 50, 0)
            trans_gate1 = rendering.Transform()
            render_gate1.add_attr(trans_gate1)
            trans_gate1.set_translation(0, 0)

            render_gate2 = rendering.make_polyline([(self.band[1], self.gate_bottom), (self.band[1], self.gate_up)])
            render_gate2.set_color(0, 50, 0)
            trans_gate2 = rendering.Transform()
            render_gate2.add_attr(trans_gate2)
            trans_gate2.set_translation(0, 0)

            self.viewer.add_geom(render_band)
            self.viewer.add_geom(render_gate1)
            self.viewer.add_geom(render_gate2)
            self.viewer.add_geom(self.agent.rendering_first())
            self.viewer.add_geom(self.enemy.rendering_first())
            self.viewer.add_geom(self.ball.rendering_first())
            # self.viewer.add_geom(self.ball2.rendering_first())
            # self.viewer.add_geom(self.ball3.rendering_first())

        # if self.state is None: return None

        self.agent.trans.set_translation(self.agent.x, self.agent.y)
        self.enemy.trans.set_translation(self.enemy.x, self.enemy.y)
        self.ball.trans.set_translation(self.ball.x, self.ball.y)
        # self.ball2.trans.set_translation(self.ball2.x, self.ball2.y)
        # self.ball3.trans.set_translation(self.ball3.x, self.ball3.y)

        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None
