import math
import numpy as np
from gym.envs.classic_control import rendering


class Objects:

    def __init__(self, x, y, r, m, color):
        self.x = x
        self.def_x = x
        self.y = y
        self.def_y = y
        self.r = r
        self.m = m
        # TODO exeptions
        self.color = color
        self.vx = 0
        self.vy = 0
        self.ax = None
        self.ay = None
        self.points = 0
        self.x_prog = 0
        self.y_prog = 0

    def rendering_first(self):

        self.render = rendering.make_circle(self.r)
        self.render.set_color(self.color[0], self.color[1], self.color[2])
        self.trans = rendering.Transform()
        self.render.add_attr(self.trans)
        self.trans.set_translation(self.x, self.y)
        return self.render

    @staticmethod
    def if_collision(o1, o2):
        p1 = np.array((o1.x, o1.y))
        p2 = np.array((o2.x, o2.y))
        d = np.sqrt(((o1.x - o2.x) ** 2) + ((o1.y - o2.y) ** 2))
        if d <= o1.r + o2.r + 1:

            return True
        else:
            return False

    @staticmethod
    def ball_collision(o1, o2):
        v1 = np.array((o1.vx, o1.vy))
        v2 = np.array((o2.vx, o2.vy))
        p1 = np.array((o1.x, o1.y))
        p2 = np.array((o2.x, o2.y))
        m1 = o1.m
        m2 = o2.m
        d = np.linalg.norm(p1 - p2) ** 2
        M = m1 + m2

        u1 = (v1 - 2 * m2 / M * np.dot(v1 - v2, p1 - p2) / d * (p1 - p2))
        u2 = v2 - 2 * m1 / M * np.dot(v2 - v1, p2 - p1) / d * (p2 - p1)

        newp = np.abs(p1 - p2)
        r_sum = o1.r + o2.r

        # TODO fix scale bug
        if o1.x <= o2.x:
            o1.x -= (r_sum - newp[0]) / 4
            o2.x += (r_sum - newp[0]) / 4
        else:
            o1.x += (r_sum - newp[0]) / 4
            o2.x -= (r_sum - newp[0]) / 4

        if o1.y <= o2.y:
            o1.y -= (r_sum - newp[1]) / 4
            o2.y += (r_sum - newp[1]) / 4
        else:
            o1.y += (r_sum - newp[1]) / 4
            o2.y -= (r_sum - newp[1]) / 4


        # vx1 = (o1.vx * (o1.m - o2.m) + (2 * o2.m * o2.vx)) / (o1.m + o2.m)
        # vy1 = (o1.vy * (o1.m - o2.m) + (2 * o2.m * o2.vy)) / (o1.m + o2.m)

        # vx2 = (o2.vx * (o2.m - o1.m) + (2 * o1.m * o1.vx)) / (o1.m + o2.m)
        # vy2 = (o2.vy * (o2.m - o1.m) + (2 * o1.m * o1.vy)) / (o1.m + o2.m)

        o1.vx, o1.vy = u1
        o2.vx, o2.vy = u2


class Players(Objects):

    def move(self, direction):
        #TODO End fisic
        if 'Fx' not in locals():
            Fx = 0
        if 'Fy' not in locals():
            Fy = 0
        v = 10
        r = 10
        dt = 0.01
        a = 10000
        # if direction == 'u':
        #     self.vy += v
        #     self.y += r
        #     print("coś")
        # if direction == 'd':
        #     self.vy -= v
        #     self.y -= r
        #
        # if direction == 'r':
        #     self.vx += v
        #     self.x += r
        #
        # if direction == 'l':
        #     self.vx -= v
        #     self.x -= r
        #
        #
        # if direction == 'ur':
        #     self.vy += v
        #     self.vx += v
        #     self.x += r
        #     self.y += r
        # if direction == 'ul':
        #     self.vy += v
        #     self.vx -= v
        #     self.y += r
        #     self.x -= r
        # if direction == 'dr':
        #     self.vy -= v
        #     self.vx += v
        #     self.y -= r
        #     self.x += r
        # if direction == 'dl':
        #     self.vy -= v
        #     self.vx -= v
        #     self.y -= r
        #     self.x -= r


        if direction == 'u':
            if self.y_prog == -1:

                self.vy = 0
                self.vx = 0

            self.y_prog = 1
            self.vx = 0
            Fy = a
            Fx = 0

        elif direction == 'd':
            if self.y_prog == 1:
                self.vy = 0
                self.vx = 0
            self.y_prog = -1
            self.vx = 0
            Fy = -a
            Fx = 0
        elif direction == 'r':
            if self.x_prog == -1:
                self.vy = 0
                self.vx = 0
            self.x_prog = 1
            self.vy = 0
            Fx = a
            Fy = 0
        elif direction == 'l':
            if self.x_prog == 1:
                self.vy = 0
                self.vx = 0
            self.x_prog = -1
            self.vy = 0
            Fx = -a
            Fy = 0

        elif direction == 'ur':
            if self.y_prog == -1:
                self.vy = 0
            if self.x_prog == -1:
                self.vx = 0
            self.x_prog = 1
            self.y_prog = 1
            Fx = a
            Fy = a
        elif direction == 'ul':
            if self.y_prog == -1:
                self.vy = 0
            if self.x_prog == 1:
                self.vx = 0
            self.x_prog = -1
            self.y_prog = 1
            Fx = -a
            Fy = a
        elif direction == 'dr':
            if self.y_prog == 1:
                self.vy = 0
            if self.x_prog == -1:
                self.vx = 0
            self.x_prog = 1
            self.y_prog = -1
            Fx = a
            Fy = -a
        elif direction == 'dl':
            if self.y_prog == 1:
                self.vy = 0
            if self.x_prog == 1:
                self.vx = 0
            self.x_prog = -1
            self.y_prog = -1
            Fx = -a
            Fy = -a
        else:
            Fx, Fy, self.vx, self.vy = 0, 0, 0, 0

        self.vx = self.vx + (Fx * dt)
        self.vy = self.vy + (Fy * dt)

        self.x = self.x + (self.vx * dt)
        self.y = self.y + (self.vy * dt)

    def band_collision(self, band, side):

        # band collision
        if side == 'l':
            if self.x - self.r <= band[0] + 1:
                self.x = band[0] + self.r + 1
                self.vx = 0

            if self.x + self.r >= (band[1] + band[0]) / 2 - 1:
                self.x = (band[1] + band[0]) / 2 - self.r - 1
                self.vx = 0

        if side == 'r':
            if self.x - self.r <= (band[1] + band[0]) / 2 + 1:
                self.x = (band[1] + band[0]) / 2 + self.r + 1
                self.vx = 0

            if self.x + self.r >= band[1] - 1:
                self.x = band[1] - self.r - 1
                self.vx = 0

        if self.y - self.r <= band[2] + 1:
            self.y = band[2] + self.r + 1
            self.vy = 0

        if self.y + self.r >= band[3] - 1:
            self.y = band[3] - self.r - 1
            self.vy = 0

    def fff(self):
        self.trans.set_translation(self.x, self.y)

    def reset(self, hard=False):
        self.x = self.def_x
        self.y = self.def_y
        self.vx = 0
        self.vy = 0
        if hard:
            self.points = 0


class Ball(Objects):

    def band_collision(self, band, gate=None):
        ns = 1  # 0.9
        """side = l(left) or r(right)
           gate = percentage size of band side
        """

        right_collision = False
        left_collision = False

        if self.x - self.r <= band[0] + 1:
            self.x = band[0] + self.r + 1
            self.vx = -self.vx * ns
            left_collision = True

        if self.x + self.r >= band[1] - 1:
            self.x = band[1] - self.r - 1
            self.vx = -self.vx * ns
            right_collision = True

        if self.y - self.r <= band[2] + 1:
            self.y = band[2] + self.r + 1
            self.vy = -self.vy * ns

        if self.y + self.r >= band[3] - 1:
            self.y = band[3] - self.r - 1
            self.vy = -self.vy * ns

        # gate collision
        if gate is not float:
            d = (band[3] - band[2]) / 2
            gate_bottom = d - (d * gate) + band[0]
            gate_up = gate_bottom + (2 * d * gate)

            if right_collision:
                if gate_up > self.y > gate_bottom:
                    print("gol r")
                    return "goal_right"
            elif left_collision:
                if gate_up > self.y > gate_bottom:
                    print("gol l")
                    return "goal_left"

    def step(self, resistance, dt):
        # self.vx = self.vx + (Fgx / self.m) * dt
        # self.vy = self.vy + (self.Fgy / self.m) * dt

        Fx = 0  # -0.5 * self.vx
        Fy = 0  # -0.4 * self.vy

        self.vx = self.vx + (Fx * dt)
        self.vy = self.vy + (Fy * dt)

        self.x = self.x + (self.vx * dt)
        self.y = self.y + (self.vy * dt)

    def reset(self, if_r_goal=False, ):
        # TODO hardcode reset
        self.x = self.def_x
        self.y = self.def_y
        self.vx = 0
        self.vy = 0
        if if_r_goal:
            self.x = 250
            self.y = 400
            self.vx = -50
            self.vy = np.random.randint(-20, 20)
        else:
            self.vx = +50
            self.vy = np.random.randint(-20, 20)
            self.x = 750
            self.y = 400
